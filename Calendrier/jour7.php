<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main>
        <div style="width: 100%;">
            <div id="corps">
                <h1 class="text-jour">JOUR 7</h1>
                <p class="contenu" style="text-align: center">
                   Coucou, alors aujourd'hui je te montre quelque-chose qui me tient beaucoup à coeur, voici une de mes musiques ! (c'est de la lo-fi)
                    <br>
                    <br>Je t'ai mis la vidéo youtube et le soundcloud pour que tu puisse choisir ta plateforme. Si t'as envie tu peux écouter mes autres musiques (principalement sur soundcloud)
                    mais je te préviens, c'est principalement du métal (miniature bleue = doux, miniature rouge = NRV)
                    <br>Je fais tout tout seul 😝 J'espère que ça te plaira !
                </p>
                <iframe height="315" src="https://www.youtube.com/embed/0WkmPWbiq54" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <iframe height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/858419872&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                <p class="contenu" style="text-align: center">
                    Si tu veux tout savoir, mon objectif à long terme est de sortir un album d'au moins 1 heure (ou alors un EP de 30 minutes) regroupant tous mes styles de musique préférés.
                    <br>Ca va du métal au boom-boom en passant par le jazz, le lo-fi et le rap.
                    C'est un projet très ambitieux qui ne plaierai pas à grand monde mais je voudrais vraiment faire découvrir la diversité la musique qu'une seule personne peut composer à travers ce projet. J'en suis actuellement à 36 minutes de musique ☺
                </p>
                <br>
                <hr>
                <br>
                <p class="contenu" style="text-align: center">
                    Aussi j'étais super content aujourd'hui parce que Claire à trouvé le pin's dans l'ancien logo du BIGG BDE, il date de 2013 (c'est chab qui l'avait fait) et il est vraiment important pour moi 😍
                    <br>Normalement tous les baptisés IUT Bourk sont censés l'avoir, mais ça fait quelques baptèmes qu'ils oublient de le donner, heureusement que Chab m'a dit que les croix IUT en avaient !
                </p>
                <img src="https://lh3.googleusercontent.com/M-XiQKbkE_RKnItk4YeaJ63spMycw1f6UU-FRXdUJFgzWxlgypmMAVk19X84wtBnr14UG3OW5dLRt4ydeLvgZZIVkW_Nt3U3zHRhkdkMYLej_blr9Ceg2mdoID2JkHdCxQofkHbTcB3NLX_H7Tp1nqdR_OipJWdMN6sQ8HBle57dc94dEwd42Ry8uPAzSilz--6kmhW-yIyOei9G3UsXkmX3ewSYt59b2hYzfJuXBaWLtLq1CnACaVd-BBivW-UQqsopHnNcraMX212jdw0EiiWXexhfmPjxY_gBO2nEwy6S0CEtfZp9RzdABkqCL4fXIq1vOnR1-xGiyJP2PueTh-aZNzEKfrECWMzKS2I5Dw0Fltdi1_mFWRzVP3c238N6oCSOsSdlFU9_7DrvdHM_GeMggk5BEK3Rv88dxvgfA2qitA9lpyqYMCaO5pn_VrhdkoYx7UyifSbnsY_OEMZgJJn5uL7dEOi6xoE8tW0ynmfcFtIeeqQtbJxo78vzKWsCykZpVtbgJ7csvTjmGaZs9SQL4Hu7cturZIPhEqS7SB6AoXflCBRTduJRrZ3oDb545n07dtgSRsVDPnYu_NK9xbQ1o1A_Jk6rRcNLNVkh4WXFI7dnHhuDboztHQd2hsaUiUh8LfXpFJEC9OQ9ZzphYTnmHvxHjZ1IkFPwMh7ffZAbmh-3BvSCU4JZ3JHOcw=w1080-h689-no?authuser=0">
                <br>
                <p class="contenu" style="text-align: center">Puis aussi j'ai encore re-commandé des pin's (ça fait 4 fois depuis novembre) mais chut 👀 </p>
                <h2 id="signature"><i>Amour & MUSIQUEEEEEEE</i> ❤🎼</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="../about.php">&Agrave; Propos</a></li>
                    <li><a href="../ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="../contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
