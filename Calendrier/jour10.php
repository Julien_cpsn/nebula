<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main>
        <div style="width: 100%;">
            <div id="corps">
                <h1 class="text-jour">JOUR 10</h1>
                <p class="contenu" style="text-align: center">
                    Voilà alors en gros j'ai blabla avec Enora toute la journée comme absolument tous les jours et elle m'a dit de dessiner ma fal.
                    <br>Sauf que y'a eu un problème, ça va surement te faire rire parce que c'est nul mais moi ça m'a NRV 😠
                    <br>(non en vrai pas du tout mais faut bien un peu de drama et de suspens sinon c'est pas drôle)*
                    <br>
                    <br>Voici mon hypothétique (peut-être) potentitelle futur fal :
                </p>
                <img src="https://media.discordapp.net/attachments/688749504337412143/786366429136945172/20201209_235907.jpg">
                <p class="contenu" style="text-align: center">
                    Et là j'étais tout colère parce que comme tu peux le voir, les couleurs de Bourk c'est vert et noir (jusque là, normal), mais les
                    couleurs du BIGG c'est fin noir sur large vert et je trouve ça super moche d'avoir deux fois les mêmes rubans, surtout que c'est
                    pas parallèle avec la partie ville de naissance. Les baptisés ayant fait partie du BIGG sont forcément des burgiens donc ils ont forcément tous le même problême !
                    <br>
                    <br>Donc je me suis empréssé d'envoyer mes meilleurs messages vocaux à Claire pour lui expliquer le problème et elle m'a dit : "va voir Chab"
                    <br>Je suis donc allé voir Chab et il m'a dit que ça l'embêtais vis à vis des futurs pious quand ils vont lire les anciennes fal du BIGG et les nouvelles, bref qu'il vont être confus si on change.
                    Et ducoup je lui ai dit qu'en une année de lecture de fal j'en encore jamais lu une seule d'un ancien membre du BIGG alors que ça allait pas perturber gand monde 👀
                    <br>
                    <br>Je suis donc parti demander l'avis de tous les impés Bourg ayant fait parti du BIGG (on est 3 x)) et j'ai demandé l'avis de Lucille, l'honoris Causae chez qui j'étais hier et qui était ancienne sécrétaire,
                    ainsi que l'avis de Chab. Et je suis reparti embêter Claire avec ça et j'ai réussi à la faire plier 😝
                    <br>
                    <br>Voici le screen :
                </p>
                <img src="https://lh3.googleusercontent.com/vcxeb_x7Ben6izofEfTxF8odlwE8minI1_90FRzr3EJ1UGw5UuA0Wr0BJYNwOoNW41Z_qrMYNpoHpGbmoBFtk9wI5u1RdVNDDCCWgefzmNXhZok4Muk0D6_JgKpnn-iSZYqg41Sqh3YWuzzKDDslDQm2zrT67idPdyoKlyKpj6kHUrhUy9QIroT8X6bwF3gEHo835VFMQkBV_HJHOcJE8hsTdI5cUXFprCdbL_UaIgVWKIYGQMMj72a7vD48xTKaR9XDRfLTo3iOEECgYQymoZDKCA_jy-FODdXYFS69BAH1Fh9cQJgUVgwsCXV53yOB7pFg76ZByC5DX4mbCRt6oSCoKb0jXIcH6snozV9IBM4f8Yym_yTp5SqLCL37zx_fSHTj7LmArT4u_e42F93Ss_c6WlN1CmXc1zDQMjS7HpM7oZuTOmVCtPgtAwforYHCOmWSJrdrRjELU1IJYQtscxEJBFbDrkJd51h2QaUmM7IOAKgviNurKnxWA-7fyu4NFVWpQ3gQzNKDC8LmiVazglU2bHLt6ABoBcwdC5nSJI1G2n4-owpIrTtA8OGhPNEmahjrjMmccm0pxmGLMXbngF5pyL7aQXCApDwGZBitGn7CW3XVbEQ8SM7r42AUXm1K52yL1IgQ316w4_Rhy9GFjXosKNC-9j3zhxVeFTCFpVXWvlF2HxVHO1WYxHLWig=w389-h937-no?authuser=0">
                <p class="contenu" style="text-align: center">
                    Et donc le ruban devrait désormais devenir fin blanc sur large rouge !!
                    <br>*bruits de Julien content* :)
                    <br>
                    <br>(j'espère que ça sera accepté 🤞)
                </p>
                <h2 id="signature"><i>Amour & cassage de couilles</i> ❤🥝</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="../about.php">&Agrave; Propos</a></li>
                    <li><a href="../ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="../contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
