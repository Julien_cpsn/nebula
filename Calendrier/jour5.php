<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main role="main" class="main">
        <div style="display: flex; align-content: center;">
            <div id="corps">
                <h1 class="text-jour">JOUR 5</h1>
                <p class="contenu" style="text-align: center">
                   Bien le bonjour, aujourd'hui tu as le droit à une liste en photo de toutes les fals que j'ai portées ! 😁
                    <br>Tout le monde me dit que c'est chiant à porter mais moi j'aime trop, à chaque fois je fais chier les gens pour porter leur fal 👀
                    <br>
                    <br>
                </p>
                <div style="display: flex; flex-wrap: wrap; vertical-align: middle;">
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784575154301698128/Snapchat-1885120685.jpg">
                        <p class="contenu" style="text-align: center"><i>Calvin - Troisb-jack</i></p>
                        <br>
                    </div>
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784575154067341342/Snapchat-1330353023.jpg">
                        <p class="contenu" style="text-align: center"><i>Loic - Bombfaya</i></p>
                        <br>
                    </div>
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784575540086571008/IMG_20200229_010544.jpg">
                        <p class="contenu" style="text-align: center"><i>Noélie - j'ai oublié son surnom</i></p>
                        <br>
                    </div>
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784575613808934932/IMG_20200229_011058.jpg">
                        <p class="contenu" style="text-align: center"><i>Noélie + Loic</i></p>
                        <br>
                    </div>
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784574793470050304/IMG_20200922_220441_129.jpg">
                        <p class="contenu" style="text-align: center"><i>Enora <3 - BrAVC</i></p>
                        <br>
                    </div>
                    <div class="img-text">
                        <img src="https://media.discordapp.net/attachments/688749504337412143/784574701287505950/IMG_20201011_030154.jpg">
                        <p class="contenu" style="text-align: center"><i>Félix - ServiePski</i></p>
                        <br>
                    </div>
                </div>
                <br>
                <p class="contenu" style="text-align: center">Voila, j'aime trop porter des fals et je pense que je suis un peu trop hypé :(
                    <br>d'ailleurs j'espère que je pourrais lire ta fal !
                    <br>dis moi si toi ça t'embête de la porter ou pas 🙂</p>
                <p class="contenu" style="text-align: center">plein de bisous et bonne journée 😗</p>
                <h2 id="signature"><i>Amour & faluche</i> ❤🎓</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="about.php">&Agrave; Propos</a></li>
                    <li><a href="ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
