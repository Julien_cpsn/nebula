<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main>
        <div style="width: 100%;">
            <div id="corps">
                <h1 class="text-jour">JOUR 12</h1>
                <p class="contenu" style="text-align: center">
                    BOJOURE LE CANARE 🦆
                    <br>
                    <br>Aujourd'hui je vais te parler de ma marraine ! (et ça va être niais [et ça sort pas trop d'ici 👀])
                    <br>Alors déjà je te l'ai déjà dit mais j'aime trop Enora, honnêtement je pouvais pas trop mieux tomber.
                    <br>
                    <br>Elle est arrivée à un moment ou ça allait pas super à cause du premier confinement (j'étais seul chez ma mère (oui)). Je lui ai direct tout dit par rapport à Océane et on en
                    a beaucoup parlé, elle m'a vraiment aidé et je lui doit pas mal de ce côté là, même si au départ je comprennais vraiment pas son délire genre je la trouvais bizarre.
                    <br>
                    <br>Mais on s'est pas mal parlé après coup, de tout et de rien et c'est un peu là que ça à commencé, depuis un peu avant septembre on se parle tous les jours (vraiment).
                    <br>On parle vraiment de tout et de rien, on s'envoit nos têtes en gros plan et on passe vraiment de très bons moments. Avec Enora c'est un peu comme une relation père fils,
                    en gros on va très rarement dire ce qu'on pense l'un de l'autre et on est très durs entre nous, on s'insulte souvent mais au fond je l'aime vraiment elle est tellement adorable.
                    <br>
                    <br>Presque tous les jours j'ai des questions sur la fal et elle prend toujours le temps de me répondre même en plein cours et elle même si on s'appelle pas souvent etc
                    c'est pas grave je sais qu'elle est toujours là quand j'en ai besoin.
                    <br>
                    <br>Après c'est aussi compliqué parce qu'elle est en couple et que honnêtement je me sens vraiment pas de lui dire que je tiens vraiment à elle souvent comme je l'aurais
                    fait avec quelqu'un d'autre, il faut savoir qu'en temps normal (pas en confinement) je suis vraiment une machine à amour et j'envois des coeur à tout le monde, mais
                    à cause des problèmes qu'il y a eu avec Océane j'ai un peur de refaire les mêmes beptises et je veux pas avoir un quelconque impact sur son couple.
                    <br>
                    <br>Sans te mentir ça m'embête d'avoir Gonzague tout le temps collé à Enora quand je l'appelle mais bon à sa place j'aurai fais un peu pareil.
                    <br>Et le résultat de tout ça c'est que je suis très très très insolent avec elle et je m'en veut parce-qu'elle mérite 40x mieux, mais je peux pas non plus
                    l'inonder d'amour parce que sinon ça sera encore très mal perçu. J'ai vraiment du mal à bien faire les choses de ce côté là.
                    <br>
                    <br>Mais sinon vraiment gros keur sur elle, j'ai tellement hâte de passer de trop bon moments en sa compagnie et qu'elle me voie évoluer grâce au travail qu'elle fourni !!
                    <br>
                    <br>Voila, je sais pas si c'était utile mais j'avais envie de te le dire pour être vraiment transparent.
                    <br>Encore une fois, passse la meilleure des journées et tout plein de bizousss ☺
                    <br>
                    <br>(Ah et ducoup révi cet aprèm 😛)
                </p>
                <h2 id="signature"><i>Amour & haine aux rats</i> ❤🐀</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="../about.php">&Agrave; Propos</a></li>
                    <li><a href="../ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="../contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
