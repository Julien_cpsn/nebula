<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main role="main" class="main">
        <div style="width: 100%; display: flex; align-content: center;">
            <div id="corps">
                <h1 class="text-jour">JOUR 4</h1>
                <p class="contenu" style="text-align: center">
                   Coucouu, ducoup j'espère que t'as passé une bonne journée toussa toussa et que ça va bien, c'est l'heure du storytime ! ☺
                    <br>
                    <br><b><i>J'ai rien à dire.</i></b>
                    <br>
                    <br>Non en vrai j'ai craqué et j'ai commandé 3 pizzas, bon heureusement Henri m'a aidé à les choisir comme tu peux le voir la photo juste en dessous
                </p>
                <img src="https://media.discordapp.net/attachments/688749504337412143/783828190535614514/20201202_235200.jpg?width=506&height=675">
                <br>
                <p class="contenu" style="text-align: center">
                    Je me suis bien pété le bide et j'ai remarqué que j'avais déjà une pizza pas finie dans mon frigo 🤦🏻‍♂️
                <br>Aussi aujourd'hui y'a les nouvelles spé IUT qui sont sorties alors voila la liste :
                    <ul>
                        <li>Couronne sur satin blanc en PP : Impetrant et/ou baptisé pendant le covid (la coronne on en a pas dans le code)</li>
                        <li>Fer à repasser : à l'endroit, se plie correctement À l'envers impliable (j’ai demandé à fournifaluche ils pourraient nous fournir)</li>
                        <li>Chauve-souris sur ruban d'asso :  mandat associatif durant le covid</li>
                        <li>Article -2 : La Faluche brûle</li>
                        <li>Epervier qui sera une mouette (Moumou la reine des) Mouette sur satin blanc : digne danseur de chorées</li>
                        <li>Marteau maillet  sur les couleurs des asso faites durant votre DUT : DUT associatif (exemple si tu fais BDE + Stud tu mets marteau maillet sur satin blanc noir jaune (dans l'ordre où tu les as faites))</li>
                    </ul>
                <p class="contenu" style="text-align: center">
                    Bon bah déjà sur les 5 nouvelles spé déjà j'ai la couronne, le fer à repasser, la chauve-souris et le marteau maillet
                    <br>ALED
                    <br>CA VA COUTER SUPER CHERE
                    <br>déjà que j'étais un peu dans le caca avec mes emblèmes de ville 🤦‍♂️
                </p>
                <p class="contenu" style="text-align: center">
                    <br>
                    <br>Question qui n'a rien à voir mais c'est pour savoir si tu suis,
                    <br>c'est quoi la première passion de Henri ? (envois un mp)
                </p>
                <p class="contenu" style="text-align: center">
                    <br>
                    <br><b>Et puis tiens une petite contrepèterie :</b>
                    <br><i>Le vieux marin enfume sa cale avec une barrette de shit et un feu de poutre.</i>
                </p>
                <h2 id="signature"><i>Amour & orthophonie</i> ❤👨‍🦽</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="about.php">&Agrave; Propos</a></li>
                    <li><a href="ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
