<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="../Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="../Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
<div class="site" style="background-color: #161616">
    <header class="hero" style="background: url('../Images/calendrier.png') no-repeat center top;">
        <div class="hero-content">
            <img id="banner-title" src="../Images/titre.png">
        </div>
    </header>
    <main>
        <div style="width: 100%;">
            <div id="corps">
                <h1 class="text-jour">JOUR 24</h1>
                <p class="contenu" style="text-align: center">
                    Voilà, on y est.
                    <br>Le 24ème jour.
                    <br>Alors déjà sache que ça va être un pavé très long et je te le dis à l'avance, y'a absolument rien d'ambigüe
                    dans ce que je vais dire !! (pour pas que ça soit ENCORE mal interprété)
                    <br>
                    <br>⚠ <strong>JE TE CONSEILLE VIVEMENT DE REGARDER TOUTES LES CASES PRÉCÉDENTES AVANT DE LIRE CELLE-CI</strong> ⚠
                    <br>
                    <br>
                    <br>
                    <br>T'es prête ?
                    <br>
                    <br>
                    <br>3...
                    <br>
                    <br>
                    <br>
                    <br>2..
                    <br>
                    <br>
                    <br>
                    <br>1.
                    <br>
                    <br>
                    <br><i>GOOOOOO</i>
                    <br>
                    <br>
                    <br>Mélie, merci d'exister. On parle maintenant depuis un peu moins d'un mois et l'année ne pouvait pas mieux
                    se terminer qu'en te rencontrant. Fal mise de côté, tu est quelqu'un d'adorable et qui "mérite" à coup sûr toutes les bonnes
                    choses qui lui arrivent. Tu est une personne remplie d'ambitions qui aime montrer son amour et partager avec les gens.
                    Je me reconnais dans ces choses et je les partages à 10000%.
                    <br>
                    <br>Pour ce qui est de la fal (c'est un peu pour ça qu'on est là hehe), je suis extrèmement content d'avoir fait
                    le choix de venir te parler, surement une de mes meilleurs décisions de l'année ! Je vais donc te raconter pourquoi.
                    <br>Déjà au star WEF je t'avais trouvée sympa même si on avait pas spécialement parlé, mais juste au feeling. Après
                    le destin à fait qu'on s'est revu au tournois des impétrants et je sais pas si tu t'en rappelle mais on s'est tout
                    de suite parlé du style "Woahhh Mélie commment ça va ?", "Trop bien et toi ??", en tous cas c'est l'impression que j'ai eu x)
                    <br>Alors que, honnêtement, on se connaissait vraiment pas, mais c'était cool ! Puis pendant le tournois des impèt,
                    j'ai trouvé ta façon de penser vraiment bien. Ce jour là j'étais chez
                    Inès, mon hypo soeurette de baptême et le soir, comme à chaque fois que je vais chez elle, on a parlé de ma recherche de PM.
                    Je lui avait parlé du fait qu'on s'était bien entendu et tout et j'ai laché en blagant "Si ça se trouve ça sera ma deuxième
                    marraine !" et elle a rigolé et elle m'a dit "en vrai ?". À ce moment là j'ai fais comme d'hab, j'essaye de dire que
                    non ça pourra pas se faire pour telle ou telle raison, et elle m'a répondu "je pense que tu devrais quand même aller lui parler".
                    Ce soir là Clément Karyo Keur mangeait avec nous et je lui en ai parlé, on a cherché dans tous ses ami FB et les miens, et
                    pas moyen de me trouver un deuxième PM, à ce moment là je me suis dis que j'allais essayer de tater le terrain avec Thomas Berger.
                    Et je sais pas trop pourquoi mais le lendemain j'avais envie de forcer un peu le destin et de me lancer dans l'inconnu alors
                    j'ai réfléchi pendant 30 minuets pour trouver une question à te poser et je l'ai envoyée sans réfléchir. (Bon bien-sûr,
                     comme toutes les questions que je t'ai posé au début, je connaissais la réponse mais je voulais voir ta vision des choses).
                    Et j'ai vraiment beaucoup accroché à tes réponses et ta vision de la fal. Et c'est bête à dire mais, je suis vraiment pas du tout
                    jaloux mais pourtant ta famille me faisait quand même un peu envie. J'ai décidé d'attendre encore un ou deux jours pour te reparler
                    et comme prévu tout s'est trop bien passé et j'étais vraiment super content. Et le 2 au soir je faisait mon DM de com et
                    pour une raison que j'ignore je me suis dit "vasy nsm je fais un calendrier à Mélie". J'ai donc tout codé en très peu de temps
                    et je me suis dis qu'on verrait bien sur le coup, je savais vraiment pas ce que je faisais ni dans quoi je m'embarquais.
                    <br>
                    <br>J'ai demandé à quelques personnes très proches d'écouter tes messages vocaux où tu dis des trucs du style "À chaque fois qu'un
                    nouveau pop dans la famille, il est toujours bien accueilli"
                    <br>"Ca spam beaucoup dans la conv j'espère que ça te dérange pas"
                    <br>"Quand quelqu'un me demande, je m'engage toujours à aller jusqu'au bout"
                    <br>"Je t'emmenerais en congrès... "
                    <br>Et toutes ces phrases, je voulais être sûr à 1000% de pas mal interpreter.
                    <br>
                    <br>Voilà, c'est en gros ce qu'il s'est passé pour en arriver jusqu'au calendrier :)
                    <br>
                    <br>Tu connais la suite.
                    <br>
                    <br>Le calendrier est maintenant fini, j'espère que tu l'as aprécié !!
                    <br>Il doit te rester plein de questions du style "et ducoup ????"
                    <br>Tu te doutes bien.
                    <br>
                    <br>J'aurais payé tellement chère pour pouvoir passer une impétrance normale, pour vous voir toi et Enora
                    sans se poser de questions, mais bon, c'est comme ça, à moi de m'adapter. Et puis disons qu'on se serait surement jamais
                    croisé sans cette soupe de bébé pangolin.
                    <br>Pour ce qui est de la situation acutelle (covid + noël), je ne ferait rien en distanciel, je n'ai pas envie que ça se passe comme ça.
                    Même si ça retarde encore les choses de mon coté, bah c'est pas grave, j'ai vraiment envie de profiter du moment où ça arrivera.
                    <br>Je tourne autour du pot depuis tout à l'heure parce que je sais que t'es curieuse et que tu peux trop pas prévoir mes prochaines lignes.
                    <br>Sâche que ce calendrier, bien qu'il m'ait prit en tout 30 heures à faire, servait juste à "prendre la température".
                    <br>Après que tu m'aies raconté les demandes de tes fillots, j'allais pas juste m'arrêter à un calendrier 😉
                    <br>Je peux pas tout expliquer parce que ça gacherais la surprise et t'imagines même pas le nombre d'heures que j'ai passé dessus.
                    C'est pas pour autant que tu dois t'attendre à un truc de ouf, tu verra bien. Bref, tout ça pour te dire (sans tact) que j'aimerais vraiment
                    énormément beaucoup qu'on se voie après que tu sois rentrée de Caen. J'ai beau avoir tout fait pour planifier quelquechose comme une
                    "surprise", c'était vraiment impossible avec le contexte actuel !
                    <br>
                    <br>
                    <br>Ah oui ettt
                    <br>
                    <br>Le contenu de la 24ème case ?
                    <br>
                    <br>
                    <br>Ahhhhh
                    <br>
                    <br>
                    <br>
                    <br>bah écoute
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>tiens, le voici :
                    <br>(oupsi 🤭)
                </p>
                <?php
                    if (!empty($_GET) && isset($_GET['rep']) && ($_GET['rep'] === 'Henrisson' || $_GET['rep'] === 'henrisson')) {
                        echo '<form method="get" style="border: 2px solid black; display: flex; flex-direction: column; align-items: center; padding-bottom: 50px; background: #444444;">
                                <p class="contenu" style="text-align: center; color: green;">CONTENU DÉVÉROUILLÉ</p>
                                <br>
                                <img style="border: none; width: 90%; transform: none;" src="../Images/Coupon.png">
                                <p class="contenu" style="text-align: center">Je te conseille vivement de l&#8217;imprimer et de le garder préciseusement !</p>
                              </form>';
                    }
                    else {
                        echo '
                        <form method="get" style="width: 40%; border: 2px solid black; display: flex; flex-direction: column; align-items: center; padding-bottom: 50px; background: #444444;">
                            <p class="contenu" style="text-align: center; color: red;">CONTENU BLOQUÉ</p>
                            <br>
                            <label for="rep">De quelle race fait partie Henri ?</label>
                            <input type="text" name="rep" id="rep" required/>
                            <button type="submit" style="margin: 10px 0 0 0; transform: none; background: black; color: white;">Valider</button>
                        </form>
                        ';
                    }
                ?>
                <br>
                <br>
                <br>
                <p class="contenu" style="text-align: center">Comme le dit si bien Mickey,</p>
                <img src="https://i.kym-cdn.com/photos/images/original/001/264/842/220.png">
                <br>
                <br>
                <br>
                <p class="contenu" style="text-align: center">(ah oui et on peut s'appeler si jamais tu veux discuter de tout ça !)</p>
                <br>
                <h2 id="signature"><i>Énormément d'amour, & FALUCHEEEEEEEEEEEE</i> ❤❤❤❤❤❤</h2>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="footer-content">
            <div class="footer-column footer-column--text">
                <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                <p>
                    Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                </p>
            </div>
            <div class="footer-column footer-column--links">
                <ul class="footer-links">
                    <li><a href="../about.php">&Agrave; Propos</a></li>
                    <li><a href="../ml.php">Mentions l&eacute;gales</a></li>
                    <li><a href="#">Remerciements</a></li>
                    <li><a href="../contact.php">Signaler un probl&egrave;me</a></li>
                    <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!--Google Analytics-->
<div class="textwidget custom-html-widget">
<!--    <script>
        (function(a, b, c) {
            var d = a.history,
                e = document,
                f = navigator || {},
                g = localStorage,
                h = encodeURIComponent,
                i = d.pushState,
                k = function() {
                    return Math.random().toString(36)
                },
                l = function() {
                    return g.cid || (g.cid = k()), g.cid
                },
                m = function(r) {
                    var s = [];
                    for (var t in r)
                        r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                    return s.join("&")
                },
                n = function(r, s, t, u, v, w, x) {
                    var z = "https://www.google-analytics.com/collect",
                        A = m({
                            v: "1",
                            ds: "web",
                            aip: c.anonymizeIp ? 1 : void 0,
                            tid: b,
                            cid: l(),
                            t: r || "pageview",
                            sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                            dr: e.referrer ||
                                void 0,
                            dt: e.title,
                            dl: e.location.origin + e.location.pathname + e.location.search,
                            ul: c.language ?
                                (f.language || "").toLowerCase() : void 0,
                            de: c.characterSet ? e.characterSet : void 0,
                            sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                            vp: c.screenSize &&
                            a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                            ec: s || void 0,
                            ea: t || void 0,
                            el: u || void 0,
                            ev: v || void 0,
                            exd: w || void 0,
                            exf: "undefined" != typeof x &&
                            !1 == !!x ? 0 : void 0
                        });
                    if (f.sendBeacon) f.sendBeacon(z, A);
                    else {
                        var y = new XMLHttpRequest;
                        y.open("POST", z, !0), y.send(A)
                    }
                };
            d.pushState = function(r) {
                return "function" == typeof d.onpushstate &&
                d.onpushstate({
                    state: r
                }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
            }, n(),
                a.ma = {
                    trackEvent: function o(r, s, t, u) {
                        return n("event", r, s, t, u)
                    },
                    trackException: function q(r, s) {
                        return n("exception", null, null, null, null, r, s)
                    }
                }
        })
        (window, "UA-4722865-4", {
            anonymizeIp: true,
            colorDepth: true,
            characterSet: true,
            screenSize: true,
            language: true
        });
    </script>-->
</div>
<!--<script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>-->
</body>
