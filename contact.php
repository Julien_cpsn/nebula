<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Neebula – Contact</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Icones -->
		<link rel="apple-touch-icon" sizes="57x57" href="Images/Icones/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="Images/Icones/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="Images/Icones/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="Images/Icones/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="Images/Icones/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="Images/Icones/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="Images/Icones/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="Images/Icones/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="Images/Icones/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="Images/Icones/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="Images/Icones/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="Images/Icones/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="Images/Icones/favicon-16x16.png">
		<link rel="manifest" href="Images/Icones/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="Images/Icones/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- CSS -->
		<link rel="stylesheet" href="CSS/styles.css">
		<link rel="stylesheet" href="CSS/contact.css">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;700&display=swap" rel="stylesheet">

		<!-- Javascript -->
		<script src="JS/scrollFadeOut.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <!-- PHP -->
        <?php include 'Include/config.php' ?>

    </head>
    <body>
        <!-- Header -->
        <?php include 'Modules/header.html' ?>

		<!-- Main -->
		<main>
			<!-- Artwork -->
			<div class="artwork">
				<img src="Images/helix.jpg" id="hubblepic" onscroll="scrollFadeOut()">
				<div>
					<h1 id="titrePage">Contact</h1>
					<hr id="underline">
				</div>
			</div>

			<!-- Breadcrumb -->
			<ul class="breadcrumb">
				<li><a href="index">Accueil</a></li>
				<li><a href="about">À propos</a></li>
				<li><a href="#">Contact</a></li>
			</ul>

			<div id="corps">
				<h2>Vous voulez me contacter ?</h2>
				<p class="paragraphe">Vous pouvez le faire par le biais de mes réseaux sociaux,</p>
				<div class="LogoRs">
					<a href="https://www.linkedin.com/in/julien-caposiena-54555a195/" target="_blank">
						<img src="Images/Logos/linkedin_logo.png" class="LogoRs" id="LinkedIn">
					</a>
					<a href="https://www.instagram.com/julien_cpsn/" target="_blank">
						<img src="Images/Logos/instagram_logo.png" class="LogoRs" id="insta">
					</a>
					<a href="https://www.facebook.com/JulienCpsn" target="_blank">
						<img src="Images/Logos/facebook_logo.png" class="LogoRs" id="fb">
					</a>
				</div>
				<p class="paragraphe">par mon adresse email <a href="mailto:julien.caposiena@gmail.com">julien.caposiena@gmail.com</a>,</p>
				<p class="paragraphe">ou par le formulaire ci-dessous</p>
				<hr>

				<h2>Formulaire de contact</h2>
				<section>
					<form class="flexbox" id="form-contact" action="Include/contact-form.php" method="POST">
						<div class="form-div" id="nom-prenom">
							<div class="form-subdiv" id="label-nom">
								<label class="form-label" for="nom">Nom*</label>
								<input class="form-input" id="nom" type="text" name="nom" required>
							</div>

							<div class="form-subdiv" id="label-prenom">
								<label class="form-label" for="prenom">Prénom*</label>
								<input class="form-input" id="prenom" type="text" name="prenom" required>
							</div>
						</div>

						<div class="form-div" id="label-email">
							<label class="form-label" for="email">Email*</label>
							<input class="form-input" id="email" type="email" name="email" required>
						</div>

						<div class="form-div" id="label-select">
							<label class="form-label" for="nature">Selectionnez la nature de votre message*</label>
							<select id="nature" name="nature" required>
								<option selected>Question générale</option>
						      	<option>Proposition stage ou alternance</option>
						      	<option>Partenariat/Sponsoring</option>
						      	<option>Question sur le site</option>
						      	<option>Autre</option>
							</select>
						</div>

						<div class="form-div" id="label-message">
							<label class="form-label" for="message">Message*</label>
							<textarea id="message" name="message" rows="12" cols="50" minlength="10" maxlength="500"  placeholder="Entrez votre message" required></textarea>
						</div>

						<div class="form-div" id="label-file">
							<label class="form-label" id="label-file" for="file">Sélectionner le fichier à envoyer <i>(optionnel)</i></label>
							<input class="form-input" id="file" multiple="false" name="file" type="file">
						</div>

                        <div class="g-recaptcha" data-sitekey="6Lc8LdYaAAAAAMwZFfYtEJmmNPOke1ZhCgKSK0eZ"></div>
                        <br/>

						<button id="submit-button" type="submit">Envoyer</button>
					</form>
				</section>
			</div>
		</main>

        <!-- Footer -->
        <?php include 'Modules/footer.html' ?>
	</body>
</html>