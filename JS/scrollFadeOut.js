window.onscroll = function() {scrollFadeOut()};

function scrollFadeOut() {
	var smartphone = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || document.getElementById('corps').clientWidth <= 800;
	console.log('Offset Y : ' + window.pageYOffset);

	if (window.pageYOffset >= 700) {
		document.getElementById('hubblepic').setAttribute('style', 'filter:brightness(0.1)');
	}
	else if (window.pageYOffset < 5) {
		document.getElementById('hubblepic').setAttribute('style', 'filter:brightness(0.8) blur(1px)');
	}
	else {
		if(!smartphone) {
    		document.getElementById('hubblepic').setAttribute('style', 'filter:brightness(' + 100/(150+window.pageYOffset) + ') blur(2px)');
    	}
    	document.getElementById('underline').setAttribute('style', 'width: ' + (25 + (window.pageYOffset*1.15 % document.getElementById('titrePage').clientWidth)) + 'px');
	}
}