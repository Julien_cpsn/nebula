<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Icones -->
		<link rel="apple-touch-icon" sizes="57x57" href="Images/Icones/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="Images/Icones/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="Images/Icones/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="Images/Icones/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="Images/Icones/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="Images/Icones/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="Images/Icones/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="Images/Icones/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="Images/Icones/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="Images/Icones/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="Images/Icones/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="Images/Icones/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="Images/Icones/favicon-16x16.png">
		<link rel="manifest" href="Images/Icones/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="Images/Icones/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- CSS -->
		<link rel="stylesheet" href="CSS/styles.css">
		<link rel="stylesheet" href="CSS/musique.css">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;700&display=swap" rel="stylesheet">

		<!-- Javascript -->
		<script src="JS/scrollFadeOut.js"></script>
		<script type="text/javascript">
			window.onload = function() {lineResize()};
			window.onresize = function() {lineResize()};

			function lineResize() {
				var width = document.getElementById('svg').clientWidth;
				var list = document.getElementsByClassName('line');

				for (let item of list) {
					item.setAttribute('x2', '' + width);
				}
			}
		</script>

        <!-- PHP -->
        <?php include 'Include/config.php' ?>

    </head>
    <body>
        <!-- Header -->
        <?php include 'Modules/header.html' ?>

        <!-- Main -->
		<main>
			<!-- Artwork -->
			<div class="artwork">
				<img src="Images/heic0503a.jpg" id="hubblepic" onscroll="scrollFadeOut()">
				<div>
					<h1 id="titrePage">Musique</h1>
					<hr id="underline">
				</div>
			</div>

			<!-- Breadcrumb -->
			<ul class="breadcrumb">
				<li><a href="index">Accueil</a></li>
				<li><a href="projets">Mes projets</a></li>
				<li><a href="#">Musique</a></li>
			</ul>

			<div id="corps">
				<h2>Liste de mes creations musicales</h2>
				<svg width="25" height="25" id="pointe">
					<polygon points="12.5 3.725, 25 25, 0 25"/>
				</svg>
				<div id="tronc">
					<div class="branche">
						<p class="date">Mars 2020</p>
						<p class="titre">Roses are red</p>
						<p class="genre">Balade rock</p>
						<svg id="svg">
							<line x1="0" y1="0" x2="200" y2="0" stroke="black" stroke-width="2" class="line" />
							<circle cx="12.5" cy="12.5" r="10" stroke="black" stroke-width="1" fill="white" />
						</svg>
						<div class="feuille">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/772448611&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>
					</div>
					<div class="branche">
						<p class="date">Avril 2020</p>
						<p class="titre">Eclipse</p>
						<p class="genre">Métal</p>
						<svg>
							<line x1="0" y1="0" x2="200" y2="0" stroke="black" stroke-width="2" class="line" />
							<circle cx="12.5" cy="12.5" r="10" stroke="black" stroke-width="1" fill="white" />
						</svg>
						<div class="feuille">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/806606839&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>
					</div>
					<div class="branche">
						<p class="date">Juin 2020</p>
						<p class="titre">Avenge, Rise!</p>
						<p class="genre">Métal</p>
						<svg>
							<line x1="0" y1="0" x2="200" y2="0" stroke="black" stroke-width="2" class="line" />
							<circle cx="12.5" cy="12.5" r="10" stroke="black" stroke-width="1" fill="white" />
						</svg>
						<div class="feuille">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/806606839&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>
					</div>
					<div class="branche">
						<p class="date">Juillet 2020</p>
						<p class="titre">Above the Broken Glass</p>
						<p class="genre">Métal</p>
						<svg>
							<line x1="0" y1="0" x2="200" y2="0" stroke="black" stroke-width="2" class="line" />
							<circle cx="12.5" cy="12.5" r="10" stroke="black" stroke-width="1" fill="white" />
						</svg>
						<div class="feuille">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/840915706&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>
					</div>
					<div class="branche">
						<p class="date">Août 2020</p>
						<p class="titre">Looking Through Night's Sky</p>
						<p class="genre">Lo-Fi</p>
						<svg>
							<line x1="0" y1="0" x2="200" y2="0" stroke="black" stroke-width="2" class="line" />
							<circle cx="12.5" cy="12.5" r="10" stroke="black" stroke-width="1" fill="white" />
						</svg>
						<div class="feuille">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/858419872&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>
					</div>
				</div>
				<hr>
				<h2>Quelques playlists</h2>
				<div id="playlists">
					<iframe src="https://open.spotify.com/embed/playlist/0spzK7A2YgwG4MW3rARHjQ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
					<iframe src="https://open.spotify.com/embed/playlist/37i9dQZF1DXbITWG1ZJKYt" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
					<iframe src="https://open.spotify.com/embed/playlist/2j0vcDb5qIdIcXPkOYSL1g" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
					<iframe src="https://open.spotify.com/embed/playlist/3yVCX2MFDDR7vwdePIvvlQ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
					<iframe src="https://open.spotify.com/embed/playlist/2TeBnBFHKf23x0ax8nCzgF" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
					<iframe src="https://open.spotify.com/embed/playlist/4lceLmAilQ9aA27vOKhA9c" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
				</div>
			</div>
		</main>

        <!-- Footer -->
        <?php include 'Modules/footer.html' ?>
	</body>

</html>