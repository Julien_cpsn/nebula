<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Neebula – À propos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Icones -->
		<link rel="apple-touch-icon" sizes="57x57" href="Images/Icones/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="Images/Icones/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="Images/Icones/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="Images/Icones/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="Images/Icones/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="Images/Icones/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="Images/Icones/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="Images/Icones/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="Images/Icones/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="Images/Icones/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="Images/Icones/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="Images/Icones/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="Images/Icones/favicon-16x16.png">
		<link rel="manifest" href="Images/Icones/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="Images/Icones/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- CSS -->
		<link rel="stylesheet" href="CSS/styles.css">
		<link rel="stylesheet" href="CSS/about.css">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;700&display=swap" rel="stylesheet">

		<!-- Javascript -->
		<script src="JS/scrollFadeOut.js"></script>

        <!-- PHP -->
        <?php include 'Include/config.php' ?>

    </head>
    <body>
        <!-- Header -->
        <?php include 'Modules/header.html' ?>

		<!-- Main -->
		<main>
			<!-- Artwork -->
			<div class="artwork">
				<img src="Images/aurore-spaciale.png" id="hubblepic" onscroll="scrollFadeOut()">
				<div>
					<h1 id="titrePage">À propos</h1>
					<hr id="underline">
				</div>
			</div>

			<!-- Breadcrumb -->
			<ul class="breadcrumb">
				<li><a href="index">Accueil</a></li>
				<li><a href="#">À propos</a></li>
			</ul>

			<div id="corps">
				<hr>
				<div class="LinkBtn">
					<div class="bouton">
						<a href="contact">
							<h2>Contact</h2>
							<img src="Images/contact.jpg">
						</a>
					</div>
					<div class="bouton">
						<a href="ml">
							<h2>Mention légales</h2>
							<img src="Images/livre.jpg">
						</a>
					</div>
				</div>
			</div>

		</main>

		<script type="text/javascript">
			if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || document.getElementById('corps').clientWidth <= 800) {
				document.getElementById('titrePage').setAttribute('style', 'font-size: 11.1vw;');
			}
		</script>

        <!-- Footer -->
        <?php include 'Modules/footer.html' ?>
	</body>
</html>