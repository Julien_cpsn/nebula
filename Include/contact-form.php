<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';
require_once 'config.php';

if (!empty($_POST)
    && isset($_POST['nom'])
    && isset($_POST['prenom'])
    && isset($_POST['email'])
    && isset($_POST['nature'])
    && isset($_POST['message'])) {

    try {
        $PDO = new PDO('mysql:host=neebulmmsg.mysql.db;port=3306;dbname=neebulmmsg', 'neebulmmsg', 'Julien2511');
        $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $statement = $PDO->prepare('INSERT INTO `messages` (date, email, nom, prenom, nature, message) VALUES (CURRENT_TIMESTAMP, :email, :nom, :prenom, :nature, :message)');

        $statement->execute(
            [
                'email' => $_POST['email'],
                'nom' => utf8_encode($_POST['nom']),
                'prenom' => utf8_encode($_POST['prenom']),
                'nature' => utf8_encode($_POST['nature']),
                'message' => utf8_encode($_POST['message'])
            ]
        );
    }
    catch (Exception $e) {
        echo 'Erreur : ' . $e;
        die();
    }

    $mail = new PHPMailer(true);
    try {
        /* DONNEES SERVEUR */
        $mail->setLanguage('fr', '../PHPMailer/language/');
        $mail->SMTPDebug = 0;
        // $mail->SMTPDebug = 2;            // décommenter en mode débug
        $mail->isSMTP();
        $mail->Host       = 'SSL0.ovh.net';
        $mail->SMTPAuth   = true;
        $mail->Username   = FORM_MAIL;
        $mail->Password   = FORM_PASSWORD;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port       = 587;

        /* DONNEES DESTINATAIRES */
        $mail->setFrom(FORM_MAIL, 'Neebula form | No-Reply');
        $mail->addAddress('julien.caposiena@gmail.com', 'Julien Cpsn');

        /* CONTENU DE L'EMAIL */
        $mail->isHTML(true);
        $mail->Subject = utf8_decode('Nouveau message du formulaire');
        $mail->Body    = '<b>Envoyé par : </b>' . $_POST['prenom'] . ' ' . $_POST['nom'] . '<br><b>Mail : </b>' . $_POST['email'] . '<br><b>Nature :</b> ' . $_POST['nature'] . '<br><b>Message : </b><br>' . $_POST['message'];

        $mail->send();
        echo 'Message envoyé.';

    }
    catch (Exception $e) {
        echo "Le Message n'a pas été envoyé. Mailer Error: {$mail->ErrorInfo}";
        die();
    }
}
header('Location:../contact.php');