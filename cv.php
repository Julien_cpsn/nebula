<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Neebula – Curriculum Vitæ</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Icones -->
        <link rel="apple-touch-icon" sizes="57x57" href="Images/Icones/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="Images/Icones/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="Images/Icones/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="Images/Icones/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="Images/Icones/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="Images/Icones/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="Images/Icones/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="Images/Icones/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="Images/Icones/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="Images/Icones/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="Images/Icones/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="Images/Icones/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="Images/Icones/favicon-16x16.png">
        <link rel="manifest" href="Images/Icones/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="Images/Icones/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- CSS -->
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/cv.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;700&display=swap" rel="stylesheet">

        <!-- Javascript -->
        <script src="JS/scrollFadeOut.js"></script>
        <script src="JS/zoom.js"></script>

        <!-- PHP -->
        <?php include 'Include/config.php' ?>

    </head>
    <body>
        <!-- Header -->
        <?php include 'Modules/header.html' ?>

        <!-- Main -->
        <main>
            <!-- Artwork -->
            <div class="artwork">
                <img src="Images/westerlund-2.jpg" id="hubblepic" onscroll="scrollFadeOut()">
                <div>
                    <h1 id="titrePage">Curriculum Vitæ</h1>
                    <hr id="underline">
                </div>
            </div>

            <!-- Breadcrumb -->
            <ul class="breadcrumb">
                <li><a href="index">Accueil</a></li>
                <li><a href="#">CV</a></li>
            </ul>

            <div id="corps">
                <div class="zoom">
                    <img src="Images/Logos/zoomIn.png" class="zoomBtn" onclick="zoomIn()">
                    <img src="Images/Logos/zoomOut.png" class="zoomBtn" onclick="zoomOut()">
                </div>
                <img src="Documents/CV_-_Julien_Caposiena.png" id="cv" alt="Photo de mon Curriculum Vitæ">
                <a href="Documents/CV_-_Julien_Caposiena.pdf" id="download" target="_blank" download>
                    <img src="Images/Logos/download.png" alt="Bouton télécharger">
                </a>
            </div>
        </main>

        <!-- Footer -->
        <?php include 'Modules/footer.html' ?>
    </body>
</html>