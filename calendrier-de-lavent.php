<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calendrier de l'avent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icones -->
    <link rel="apple-touch-icon" sizes="57x57" href="Images/Icones/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="Images/Icones/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="Images/Icones/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="Images/Icones/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="Images/Icones/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="Images/Icones/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="Images/Icones/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="Images/Icones/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="Images/Icones/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="Images/Icones/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="Images/Icones/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="Images/Icones/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="Images/Icones/favicon-16x16.png">
    <link rel="manifest" href="Images/Icones/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="Images/Icones/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="https://www.24joursdeweb.fr/wp-content/themes/24jdw2020/style.css" />
    <link rel="stylesheet" href="../CSS/calendrier.css" />

</head>
<body class="home blog edition--2020 home--2020 home--new">
	<div class="site" style="background-color: #252525">
        <header class="hero" style="background: url('Images/calendrier.png') no-repeat center top;">
			<div class="hero-content">
                <img id="banner-title" src="Images/titre.png">
            </div>
		</header>
        <main role="main" class="main">
            <ol class="calendar">
                <li class="day">
                    <a class="day-link" href="Calendrier/jour1.php">
                        <span class="day-number">01</span>
                        <span class="day-title">La première case du calendrier (jusque là on sait compter c'est cool)</span>
                        <span class="day-author">en retard</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour2.php">
                        <span class="day-number">02</span>
                        <span class="day-title">g pa le temp</span>
                        <span class="day-author">par Aplui (lol)</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour3.php">
                        <span class="day-number">03</span>
                        <span class="day-title">Henri, brave guerrier des temps modernes</span>
                        <span class="day-author">par Henri</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour4.php">
                        <span class="day-number">04</span>
                        <span class="day-title">Storytime
                            <br>&
                            <br>contrepèteries</span>
                        <span class="day-author">Il est actuellement 02:02 et j'ai envie de dodoooo</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour5.php">
                        <span class="day-number">05</span>
                        <span class="day-title">Porte-faluche</span>
                        <span class="day-author">il est actuellement 3h18</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour6.php">
                        <span class="day-number">06</span>
                        <span class="day-title">Profonde réflexion</span>
                        <span class="day-author">par Julien le petit pitre</span>
                    </a>
                </li>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour7.php">
                        <span class="day-number">07</span>
                        <span class="day-title">Musique & pin's</span>
                        <span class="day-author">c'est dur la vie d'artiste</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour8.php">
                        <span class="day-number">08</span>
                        <span class="day-title">Gaga de gaga</span>
                        <span class="day-author">Je suis bcp trop fatigué</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour9.php">
                        <span class="day-number">09</span>
                        <span class="day-title">Mon cantum</span>
                        <span class="day-author">par moi même (en personne)</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour10.php">
                        <span class="day-number">10</span>
                        <span class="day-title">LÉ RUBAN C PA BO 😠</span>
                        <span class="day-author">par un homme NRV</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour11.php">
                        <span class="day-number">11</span>
                        <span class="day-title">C'est pas la case la plus drôle 🙄</span>
                        <span class="day-author">en plus ça parle pas de fal</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour12.php">
                        <span class="day-number">12</span>
                        <span class="day-title">Haine aux rats</span>
                        <span class="day-author">reine des zouz</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour13.php">
                        <span class="day-number">13</span>
                        <span class="day-title">Musique Maestro !</span>
                        <span class="day-author">par DJ Nocide</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour14.php">
                        <span class="day-number">14</span>
                        <span class="day-title">Chinoiseries</span>
                        <span class="day-author">par le syndrome de la page blanche</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour15.php">
                        <span class="day-number">15</span>
                        <span class="day-title">Qu'est-ce que ? 😮</span>
                        <span class="day-author">Mystère et boule de gomme</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour16.php">
                        <span class="day-number">16</span>
                        <span class="day-title">Ma sale gueule</span>
                        <span class="day-author">par un homme moche</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour17.php">
                        <span class="day-number">17</span>
                        <span class="day-title">C'est l'heure de s'envoyer en l'air</span>
                        <span class="day-author">par un homme au 7ème ciel </span>🤭
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour18.php">
                        <span class="day-number">18</span>
                        <span class="day-title">GOODIES</span>
                        <span class="day-author">par UN HOMME LIBRE QU'A FINI SES DS</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour19.php">
                        <span class="day-number">19</span>
                        <span class="day-title">Aurtofonnie</span>
                        <span class="day-author">par quelqu'un de curieux !</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour20.php">
                        <span class="day-number">20</span>
                        <span class="day-title">L'heure du partiel</span>
                        <span class="day-author">par Albus Dumbledor</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour21.php">
                        <span class="day-number">21</span>
                        <span class="day-title">Remontant</span>
                        <span class="day-author">par la remontrance</span>
                    </a>
                </li>
                    <li class="day-off">
                        <p style="text-align: left; position: absolute; margin: 120px 0 50% 0;">rien.</p>
                    <span class="day-off-cell"><span class="day-off-number">22</span></span>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/jour23.php">
                        <span class="day-number">23</span>
                        <span class="day-title">CURIOSITÉ QUAND TU NOUS TIENT</span>
                        <span class="day-author">des tensions se créent dans la villa</span>
                    </a>
                </li>
                <li class="day">
                    <a class="day-link" href="Calendrier/dernierjour.php">
                        <span class="day-number">24</span>
                        <img src="https://assets.stickpng.com/images/580b585b2edbce24c47b2415.png" style="width: 80%; margin-top: -10px; margin-left: 50%; transform: translateX(-50%);">
                    </a>
                </li>
                </ol>
        </main>
        <footer class="footer">
            <div class="footer-content">
                <div class="footer-column footer-column--text">
                    <p class="footer-baseline">Calendrier de l&rsquo;avent pour Mélie (enfin je crois).</p>
                    <p>2020, tous droits r&eacute;serv&eacute;s.</p>
                    <p>
                        Illustration par <a href="https://www.instagram.com/julien_cpsn/" target="_blank">Julien</a> (non je déconne).
                    </p>
                </div>
                <div class="footer-column footer-column--links">
                    <ul class="footer-links">
                        <li><a href="about.php">&Agrave; Propos</a></li>
                        <li><a href="ml.php">Mentions l&eacute;gales</a></li>
                        <li><a href="#">Remerciements</a></li>
                        <li><a href="contact.php">Signaler un probl&egrave;me</a></li>
                        <li class="footer-link--twitter"><a href="https://www.twitter.com/julien_cpsn" target="_blank">@julien_cpsn</a></li>
                    </ul>
                </div>
            </div>
        </footer>
	</div>
			<!--Google Analytics-->
    <div class="textwidget custom-html-widget">
        <script>
            (function(a, b, c) {
                var d = a.history,
                    e = document,
                    f = navigator || {},
                    g = localStorage,
                    h = encodeURIComponent,
                    i = d.pushState,
                    k = function() {
                        return Math.random().toString(36)
                    },
                    l = function() {
                        return g.cid || (g.cid = k()), g.cid
                    },
                    m = function(r) {
                        var s = [];
                        for (var t in r)
                            r.hasOwnProperty(t) && void 0 !== r[t] && s.push(h(t) + "=" + h(r[t]));
                        return s.join("&")
                    },
                    n = function(r, s, t, u, v, w, x) {
                        var z = "https://www.google-analytics.com/collect",
                            A = m({
                                v: "1",
                                ds: "web",
                                aip: c.anonymizeIp ? 1 : void 0,
                                tid: b,
                                cid: l(),
                                t: r || "pageview",
                                sd: c.colorDepth && screen.colorDepth ? screen.colorDepth + "-bits" : void 0,
                                dr: e.referrer ||
                                    void 0,
                                dt: e.title,
                                dl: e.location.origin + e.location.pathname + e.location.search,
                                ul: c.language ?
                                    (f.language || "").toLowerCase() : void 0,
                                de: c.characterSet ? e.characterSet : void 0,
                                sr: c.screenSize ? (a.screen || {}).width + "x" + (a.screen || {}).height : void 0,
                                vp: c.screenSize &&
                                a.visualViewport ? (a.visualViewport || {}).width + "x" + (a.visualViewport || {}).height : void 0,
                                ec: s || void 0,
                                ea: t || void 0,
                                el: u || void 0,
                                ev: v || void 0,
                                exd: w || void 0,
                                exf: "undefined" != typeof x &&
                                !1 == !!x ? 0 : void 0
                            });
                        if (f.sendBeacon) f.sendBeacon(z, A);
                        else {
                            var y = new XMLHttpRequest;
                            y.open("POST", z, !0), y.send(A)
                        }
                    };
                d.pushState = function(r) {
                    return "function" == typeof d.onpushstate &&
                    d.onpushstate({
                        state: r
                    }), setTimeout(n, c.delay || 10), i.apply(d, arguments)
                }, n(),
                    a.ma = {
                        trackEvent: function o(r, s, t, u) {
                            return n("event", r, s, t, u)
                        },
                        trackException: function q(r, s) {
                            return n("exception", null, null, null, null, r, s)
                        }
                    }
            })
            (window, "UA-4722865-4", {
                anonymizeIp: true,
                colorDepth: true,
                characterSet: true,
                screenSize: true,
                language: true
            });
        </script>
    </div>
    <script type='text/javascript' src='https://www.24joursdeweb.fr/wp-includes/js/wp-embed.min.js?ver=4.9.16'></script>
</body>